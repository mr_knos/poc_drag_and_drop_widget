import 'package:flutter/material.dart';
import 'package:poc_drag_and_drop_widget/models/item.dart';

class ItemWidget extends StatelessWidget {
  const ItemWidget({
    required this.title,
    this.color,
    this.size,
    Key? key,
  }) : super(key: key);

  factory ItemWidget.fromItem(Item item) {
    return ItemWidget(
      title: item.title,
      color: item.color,
      size: item.size,
    );
  }

  final String title;
  final Color? color;
  final Size? size;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(4),
      constraints: BoxConstraints(
        maxWidth: size?.width ?? 180,
        maxHeight: size?.height ?? 180,
      ),
      alignment: Alignment.center,
      color: color,
      child: Text(title, textScaleFactor: 2),
    );
  }
}
