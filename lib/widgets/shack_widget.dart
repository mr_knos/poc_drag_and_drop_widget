import 'dart:math';

import 'package:flutter/material.dart';

class ShackWidget extends StatefulWidget {
  const ShackWidget({
    required this.child,
    this.deltaX = 0.6,
    this.deltaY = 0,
    this.velocity = 50,
    Key? key,
  }) : super(key: key);

  final Widget child;
  final double deltaX;
  final double deltaY;
  final double velocity;

  @override
  State<ShackWidget> createState() => _ShackWidgetState();
}

class _ShackWidgetState extends State<ShackWidget>
    with TickerProviderStateMixin {
  late final AnimationController _controller = AnimationController(
    duration: const Duration(seconds: 10),
    vsync: this,
  )..repeat();

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: AnimatedBuilder(
        animation: _controller,
        child: widget.child,
        builder: (BuildContext context, Widget? child) {
          final delta = sin(widget.velocity * 2 * pi * _controller.value);

          return Transform.translate(
            offset: Offset(
              delta * widget.deltaX,
              delta * widget.deltaY,
            ),
            child: child,
          );
        },
      ),
    );
  }
}
