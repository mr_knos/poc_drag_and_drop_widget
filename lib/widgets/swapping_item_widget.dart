import 'package:flutter/material.dart';

class SwappingWidget extends StatefulWidget {
  const SwappingWidget({
    required this.color,
    Key? key,
  }) : super(key: key);

  final Color color;

  @override
  State<SwappingWidget> createState() => _SwappingWidgetState();
}

class _SwappingWidgetState extends State<SwappingWidget> {
  Size get widgetSize => const Size(180, 180);
  Widget? _droppedWidget;

  @override
  Widget build(BuildContext context) {
    return _buildDropAbleWidget(
      widget: _buildDraggableWidget(),
    );
  }

  Widget _buildDropAbleWidget({
    required Widget widget,
  }) {
    return DragTarget<Widget>(
      builder: (context, accepted, rejected) => _droppedWidget ?? widget,
      onWillAccept: (_) => true,
      onAccept: (droppedWidget) => setState(
        () => _droppedWidget = droppedWidget,
      ),
    );
  }

  Widget _buildDraggableWidget() {
    return LongPressDraggable<Widget>(
      data: _buildItem(color: widget.color),
      child: _buildItem(color: widget.color),
      feedback: _buildItemFeedback(),
      childWhenDragging: _buildItem(
        color: widget.color.withOpacity(0.5),
      ),
      onDragCompleted: () {
        debugPrint('Drag complete');
      },
      onDraggableCanceled: (velocity, offset) {
        debugPrint('Drag cancel');
      },
    );
  }

  Widget _buildItemScaffold({
    required String title,
    Color? color,
  }) {
    return Container(
      height: widgetSize.height,
      width: widgetSize.width,
      color: color ?? widget.color,
      child: Center(
        child: Text(
          title,
          textScaleFactor: 2,
          style: const TextStyle(color: Colors.white),
        ),
      ),
    );
  }

  Widget _buildItem({required Color color}) {
    return _buildItemScaffold(
      title: 'Drag me',
      color: color,
    );
  }

  Widget _buildItemFeedback() {
    return Material(
      child: _buildItemScaffold(title: 'Dragging'),
    );
  }
}
