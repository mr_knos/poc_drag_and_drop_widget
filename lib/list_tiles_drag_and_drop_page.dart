import 'dart:math';

import 'package:flutter/material.dart';
import 'package:poc_drag_and_drop_widget/models/item.dart';

class ListTilesDragAndDropPage extends StatefulWidget {
  const ListTilesDragAndDropPage({Key? key}) : super(key: key);

  @override
  _ListTilesDragAndDropPageState createState() =>
      _ListTilesDragAndDropPageState();
}

class _ListTilesDragAndDropPageState extends State<ListTilesDragAndDropPage> {
  final items = List.generate(
    10,
    (index) => Item(
      title: index.toString(),
      color: Color((Random().nextDouble() * 0xFFFFFF).toInt()).withOpacity(0.8),
    ),
  );

  void _onReorder(int oldIndex, int newIndex) {
    setState(() {
      final index = newIndex > oldIndex ? newIndex - 1 : newIndex;

      final item = items.removeAt(oldIndex);
      items.insert(index, item);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('List Tiles drag and drop'),
      ),
      body: ReorderableListView.builder(
        itemBuilder: (context, index) => _buildTile(
          context,
          key: ValueKey(index),
          item: items.elementAt(index),
        ),
        itemCount: items.length,
        onReorder: _onReorder,
      ),
    );
  }

  Widget _buildTile(
    BuildContext context, {
    required Key key,
    required Item item,
  }) {
    return Container(
      key: ValueKey(item),
      height: 50,
      padding: const EdgeInsets.symmetric(horizontal: 20),
      alignment: Alignment.centerLeft,
      color: item.color,
      child: Text(item.title),
    );
  }
}
