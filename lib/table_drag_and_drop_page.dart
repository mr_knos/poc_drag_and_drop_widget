import 'package:flutter/material.dart';
import 'package:poc_drag_and_drop_widget/widgets/item_widget.dart';
import 'package:reorderables/reorderables.dart';

class TableDragAndDropPage extends StatefulWidget {
  const TableDragAndDropPage({Key? key}) : super(key: key);

  @override
  State<TableDragAndDropPage> createState() => _TableDragAndDropPageState();
}

class _TableDragAndDropPageState extends State<TableDragAndDropPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Table drag and drop'),
      ),
      body: const TableExample(),
    );
  }
}

class TableExample extends StatefulWidget {
  const TableExample({Key? key}) : super(key: key);

  @override
  _TableExampleState createState() => _TableExampleState();
}

class _TableExampleState extends State<TableExample> {
  late final List<ReorderableTableRow> _itemRows;

  @override
  void initState() {
    super.initState();

    final data = [
      ['11', '12', '13'],
      ['21', '22', '23'],
      ['31', '32', '33'],
    ];

    _itemRows = data.map((row) {
      return ReorderableTableRow(
        key: ObjectKey(row),
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          ItemWidget(title: row[0]),
          ItemWidget(title: row[1]),
          ItemWidget(title: row[2]),
        ],
      );
    }).toList();
  }

  void _onReorder(int oldIndex, int newIndex) {
    setState(() {
      ReorderableTableRow row = _itemRows.removeAt(oldIndex);
      _itemRows.insert(newIndex, row);
    });
  }

  @override
  Widget build(BuildContext context) {
    return ReorderableTable(
      header: _buildHeader(),
      children: _itemRows,
      onReorder: _onReorder,
      onNoReorder: (int index) {
        debugPrint(
          '${DateTime.now().toString().substring(5, 22)} reorder cancelled. index:$index',
        );
      },
    );
  }

  Widget _buildHeader() {
    return ReorderableTableRow(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: const [
        Text('A', textScaleFactor: 1.5),
        Text('B', textScaleFactor: 1.5),
        Text('C', textScaleFactor: 1.5),
      ],
    );
  }
}
