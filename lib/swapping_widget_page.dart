import 'package:flutter/material.dart';
import 'package:poc_drag_and_drop_widget/models/item.dart';
import 'package:reorderables/reorderables.dart';

class SwappingWidgetPage extends StatefulWidget {
  const SwappingWidgetPage({Key? key}) : super(key: key);

  @override
  State<SwappingWidgetPage> createState() => _SwappingWidgetPageState();
}

class _SwappingWidgetPageState extends State<SwappingWidgetPage> {
  final items = [
    Item.random(
      title: 'Home',
      color: Colors.redAccent,
    ),
    Item.random(
      title: 'Notification',
      color: Colors.greenAccent,
    ),
    Item.random(
      title: 'Calendar',
      color: Colors.yellow,
    ),
    Item.random(
      title: 'Home health',
      color: Colors.purpleAccent,
    ),
  ];

  void _onReorder(int oldIndex, int newIndex) {
    setState(() {
      final row = items.removeAt(oldIndex);
      items.insert(newIndex, row);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Swapping widget'),
      ),
      body: _buildReorder(context),
    );
  }

  Widget _buildReorder(BuildContext context) {
    return ReorderableWrap(
      onReorder: _onReorder,
      children: items
          .map((item) => _buildItem(
                context,
                item: item,
                isSmall: item.title != 'Calendar',
              ))
          .toList(),
    );
  }

  Widget _buildItem(
    BuildContext context, {
    required Item item,
    required bool isSmall,
  }) {
    final maxWidth = MediaQuery.of(context).size.width;

    return Container(
      constraints: BoxConstraints(
        maxWidth: isSmall ? maxWidth / 2 : maxWidth,
        maxHeight: 180,
      ),
      child: Container(
        width: double.maxFinite,
        height: double.maxFinite,
        color: item.color,
        alignment: Alignment.center,
        child: Text(item.title, textScaleFactor: 2),
      ),
    );
  }
}
