import 'package:flutter/material.dart';
import 'package:poc_drag_and_drop_widget/advance_drag_and_drop_page.dart';
import 'package:poc_drag_and_drop_widget/column_drag_and_drop_page.dart';
import 'package:poc_drag_and_drop_widget/row_drag_and_drop_page.dart';
import 'package:poc_drag_and_drop_widget/simple_drag_and_drop_page.dart';
import 'package:poc_drag_and_drop_widget/swapping_widget_page.dart';
import 'package:poc_drag_and_drop_widget/table_drag_and_drop_page.dart';
import 'package:poc_drag_and_drop_widget/wrap_drag_and_drop_page.dart';

import 'list_tiles_drag_and_drop_page.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key}) : super(key: key);

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Home'),
      ),
      body: Column(
        children: [
          const SizedBox(height: 20),
          _buildPageNavigatorButton(
            context,
            title: 'Simple drag and drop',
            page: const SimpleDragAndDropPage(),
          ),
          _buildPageNavigatorButton(
            context,
            title: 'List Tiles drag and drop',
            page: const ListTilesDragAndDropPage(),
          ),
          _buildPageNavigatorButton(
            context,
            title: 'Wrap drag and drop',
            page: const WrapDragAndDropPage(),
          ),
          _buildPageNavigatorButton(
            context,
            title: 'Row drag and drop',
            page: const RowDragAndDropPage(),
          ),
          _buildPageNavigatorButton(
            context,
            title: 'Column drag and drop',
            page: const ColumnDragAndDropPage(),
          ),
          _buildPageNavigatorButton(
            context,
            title: 'Table drag and drop',
            page: const TableDragAndDropPage(),
          ),
          _buildPageNavigatorButton(
            context,
            title: 'Swapping widget',
            page: const SwappingWidgetPage(),
          ),
          _buildPageNavigatorButton(
            context,
            title: 'Advance drag and drop',
            page: const AdvanceDragAndDropPage(),
          ),
        ],
      ),
    );
  }

  Widget _buildPageNavigatorButton(
    BuildContext context, {
    required String title,
    required Widget page,
  }) {
    return Center(
      child: ElevatedButton(
        onPressed: () => Navigator.push(
          context,
          MaterialPageRoute(builder: (_) => page),
        ),
        child: Text(title, textScaleFactor: 1.2),
      ),
    );
  }
}
