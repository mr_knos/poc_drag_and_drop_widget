import 'package:flutter/material.dart';
import 'package:reorderables/reorderables.dart';

class RowDragAndDropPage extends StatelessWidget {
  const RowDragAndDropPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Row drag and drop'),
      ),
      body: const RowExample(),
    );
  }
}

class RowExample extends StatefulWidget {
  const RowExample({Key? key}) : super(key: key);

  @override
  _RowExampleState createState() => _RowExampleState();
}

class _RowExampleState extends State<RowExample> {
  late List<Widget> _columns;

  @override
  void initState() {
    super.initState();
    _columns = [
      Image.asset('assets/river1.jpg', key: const ValueKey('river1.jpg')),
      Image.asset('assets/river2.jpg', key: const ValueKey('river2.jpg')),
      Image.asset('assets/river3.jpg', key: const ValueKey('river3.jpg')),
    ];
  }

  @override
  Widget build(BuildContext context) {
    void _onReorder(int oldIndex, int newIndex) {
      setState(() {
        Widget col = _columns.removeAt(oldIndex);
        _columns.insert(newIndex, col);
      });
    }

    return ReorderableRow(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: _columns,
      onReorder: _onReorder,
      onNoReorder: (int index) {
        //this callback is optional
        debugPrint(
          '${DateTime.now().toString().substring(5, 22)} reorder cancelled. index:$index',
        );
      },
    );
  }
}
