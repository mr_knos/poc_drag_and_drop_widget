import 'package:flutter/material.dart';

enum Size { small, medium, large }

class HomeWidget {
  HomeWidget({
    required this.title,
    required this.color,
    required this.size,
    this.leftWidget,
    this.rightWidget,
  });

  final String title;
  final Color color;
  final HomeWidget? leftWidget;
  final HomeWidget? rightWidget;
  final Size size;
}

class WeatherWidget extends HomeWidget {
  WeatherWidget({
    HomeWidget? leftWidget,
    HomeWidget? rightWidget,
  }) : super(
          title: 'Weather',
          size: Size.small,
          color: Colors.blueAccent,
          leftWidget: leftWidget,
          rightWidget: rightWidget,
        );
}

class NotificationWidget extends HomeWidget {
  NotificationWidget({
    HomeWidget? leftWidget,
    HomeWidget? rightWidget,
  }) : super(
          title: 'Notification',
          size: Size.small,
          color: Colors.greenAccent,
          leftWidget: leftWidget,
          rightWidget: rightWidget,
        );
}

class CalendarWidget extends HomeWidget {
  CalendarWidget({
    HomeWidget? leftWidget,
    HomeWidget? rightWidget,
  }) : super(
          title: 'Calendar',
          size: Size.medium,
          color: Colors.redAccent,
          leftWidget: leftWidget,
          rightWidget: rightWidget,
        );
}

class HomeHealthWidget extends HomeWidget {
  HomeHealthWidget({
    HomeWidget? leftWidget,
    HomeWidget? rightWidget,
  }) : super(
          title: 'HomeHealth',
          size: Size.small,
          color: Colors.purpleAccent,
          leftWidget: leftWidget,
          rightWidget: rightWidget,
        );
}

class BillPaymentWidget extends HomeWidget {
  BillPaymentWidget({
    HomeWidget? leftWidget,
    HomeWidget? rightWidget,
  }) : super(
          title: 'BillPayment',
          size: Size.small,
          color: Colors.yellowAccent,
          leftWidget: leftWidget,
          rightWidget: rightWidget,
        );
}

class EmptyWidget extends HomeWidget {
  EmptyWidget({
    required Size size,
    HomeWidget? leftWidget,
    HomeWidget? rightWidget,
  }) : super(
          title: 'Empty',
          size: size,
          color: Colors.yellowAccent,
          leftWidget: leftWidget,
          rightWidget: rightWidget,
        );
}

class GroupWidget extends HomeWidget {
  GroupWidget({
    HomeWidget? leftWidget,
    HomeWidget? rightWidget,
  }) : super(
          title: 'Group',
          size: Size.small,
          color: Colors.transparent,
          leftWidget: leftWidget,
          rightWidget: rightWidget,
        );
}
