import 'dart:math';
import 'dart:ui';

class Item {
  Item({
    required this.title,
    required this.color,
    this.size,
  });

  factory Item.random({
    String? title,
    Color? color,
    Size? size,
  }) {
    return Item(
      title: title ?? (Random().nextDouble() % 100).toString(),
      color: color ??
          Color(
            (Random().nextDouble() * 0xFFFFFF).toInt(),
          ).withOpacity(0.8),
      size: size ?? const Size(180, 180),
    );
  }

  final String title;
  final Color color;
  final Size? size;
}
