import 'package:flutter/material.dart';
import 'package:poc_drag_and_drop_widget/models/item.dart';
import 'package:poc_drag_and_drop_widget/widgets/shack_widget.dart';
import 'package:reorderables/reorderables.dart';

class AdvanceDragAndDropPage extends StatefulWidget {
  const AdvanceDragAndDropPage({Key? key}) : super(key: key);

  @override
  State<AdvanceDragAndDropPage> createState() => _AdvanceDragAndDropPageState();
}

class _AdvanceDragAndDropPageState extends State<AdvanceDragAndDropPage> {
  bool isEditing = false;

  final items = [
    Item.random(
      title: 'Home',
      color: Colors.redAccent,
    ),
    Item.random(
      title: 'Notification',
      color: Colors.greenAccent,
    ),
    Item.random(
      title: 'Calendar',
      color: Colors.yellow,
    ),
    Item.random(
      title: 'Home health',
      color: Colors.purpleAccent,
    ),
  ];

  void _onReorder(int oldIndex, int newIndex) {
    setState(() {
      final row = items.removeAt(oldIndex);
      items.insert(newIndex, row);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Advance drag and drop'),
        actions: [
          TextButton(
            onPressed: () => setState(() => isEditing = !isEditing),
            child: Text(
              isEditing ? 'Done' : 'Edit',
              style: const TextStyle(color: Colors.white),
              textScaleFactor: 1.5,
            ),
          ),
        ],
      ),
      body: _buildReorder(context),
    );
  }

  Widget _buildReorder(BuildContext context) {
    return ReorderableWrap(
      needsLongPressDraggable: !isEditing,
      onReorder: _onReorder,
      onReorderStarted: (_) => setState(() => isEditing = true),
      children: items
          .map((item) => _buildItem(
                context,
                item: item,
                isSmall: item.title != 'Calendar',
              ))
          .toList(),
    );
  }

  Widget _buildItem(
    BuildContext context, {
    required Item item,
    required bool isSmall,
  }) {
    return _buildReorderDecoration(
      context,
      isSmall: isSmall,
      child: Container(
        margin: const EdgeInsets.all(8),
        width: double.maxFinite,
        height: double.maxFinite,
        color: item.color,
        alignment: Alignment.center,
        child: Text(item.title, textScaleFactor: 2),
      ),
    );
  }

  Widget _buildReorderDecoration(
    BuildContext context, {
    required bool isSmall,
    required Widget child,
  }) {
    final maxWidth = MediaQuery.of(context).size.width;

    return Container(
      constraints: BoxConstraints(
        maxWidth: isSmall ? maxWidth / 2 : maxWidth,
        maxHeight: 180,
      ),
      child: isEditing ? ShackWidget(child: child) : child,
    );
  }
}
