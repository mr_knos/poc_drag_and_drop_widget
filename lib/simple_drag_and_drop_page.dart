import 'package:flutter/material.dart';

class SimpleDragAndDropPage extends StatefulWidget {
  const SimpleDragAndDropPage({Key? key}) : super(key: key);

  @override
  State<SimpleDragAndDropPage> createState() => _SimpleDragAndDropPageState();
}

class _SimpleDragAndDropPageState extends State<SimpleDragAndDropPage> {
  bool _isDropped = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Simple drag and drop'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Center(
              child: LongPressDraggable<String>(
                data: 'red',
                child: Container(
                  height: 150.0,
                  width: 150.0,
                  color: Colors.redAccent,
                  child: const Center(
                    child: Text(
                      'Drag me',
                      textScaleFactor: 2,
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                ),
                feedback: Material(
                  child: Container(
                    height: 170.0,
                    width: 170.0,
                    decoration: const BoxDecoration(
                      color: Colors.redAccent,
                    ),
                    child: const Center(
                      child: Text(
                        'Dragging',
                        textScaleFactor: 2,
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                  ),
                ),
                childWhenDragging: Container(
                  height: 120,
                  width: 120,
                  color: Colors.green,
                ),
                onDragCompleted: () {
                  debugPrint('Drag complete');
                },
                onDraggableCanceled: (velocity, offset) {
                  debugPrint('Drag cancel');
                },
              ),
            ),
            SizedBox(height: MediaQuery.of(context).size.height * 0.1),
            DragTarget<String>(
              builder: (
                context,
                accepted,
                rejected,
              ) {
                return Container(
                  height: 200,
                  width: 200,
                  color: _isDropped ? Colors.red : Colors.grey.shade400,
                  child: Center(
                    child: Text(
                      _isDropped ? 'Dropped' : 'Drop here',
                      textScaleFactor: 2,
                      style: const TextStyle(color: Colors.white),
                    ),
                  ),
                );
              },
              onWillAccept: (data) => data == 'red',
              onAccept: (data) => setState(() => _isDropped = true),
            ),
            SizedBox(height: MediaQuery.of(context).size.height * 0.1),
            ElevatedButton(
              onPressed: () => setState(() => _isDropped = false),
              child: const Text('Reset'),
            ),
          ],
        ),
      ),
    );
  }
}
