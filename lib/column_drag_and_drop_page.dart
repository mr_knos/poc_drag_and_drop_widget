import 'dart:math';

import 'package:flutter/material.dart';
import 'package:poc_drag_and_drop_widget/widgets/item_widget.dart';
import 'package:reorderables/reorderables.dart';

class ColumnDragAndDropPage extends StatelessWidget {
  const ColumnDragAndDropPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Column drag and drop'),
      ),
      body: const ColumnExample(),
    );
  }
}

class ColumnExample extends StatefulWidget {
  const ColumnExample({Key? key}) : super(key: key);

  @override
  _ColumnExampleState createState() => _ColumnExampleState();
}

class _ColumnExampleState extends State<ColumnExample> {
  final List<Widget> _items = List.generate(
    10,
    (index) => ItemWidget(
      key: ValueKey(index),
      title: index.toString(),
      color: Color((Random().nextDouble() * 0xFFFFFF).toInt()).withOpacity(0.8),
    ),
  );

  @override
  Widget build(BuildContext context) {
    void _onReorder(int oldIndex, int newIndex) {
      setState(() {
        Widget item = _items.removeAt(oldIndex);
        _items.insert(newIndex, item);
      });
    }

    return ReorderableColumn(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: _items,
      onReorder: _onReorder,
      onNoReorder: (int index) {
        //this callback is optional
        debugPrint(
          '${DateTime.now().toString().substring(5, 22)} reorder cancelled. index:$index',
        );
      },
    );
  }
}
